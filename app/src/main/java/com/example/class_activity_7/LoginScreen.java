package com.example.class_activity_7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class LoginScreen extends AppCompatActivity {

    CheckBox checkBox;
    SharedPreferences s;
    EditText username,password;
    Button in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        checkBox=findViewById(R.id.checkBox);
        username=findViewById(R.id.name);
        password=findViewById(R.id.pass);
        in=findViewById(R.id.button);

        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginScreen.this, MainActivity.class));
            }
        });
        s = (SharedPreferences) getSharedPreferences("MySharedPref",MODE_PRIVATE);
        SharedPreferences.Editor myEdit =s.edit();
        if(getSharedPreferences("MySharedPref",MODE_PRIVATE)==null)
        {
            checkBox.setActivated(false);
        }
        else
        {
            checkBox.setActivated(true);
        }

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked=((CheckBox) checkBox).isChecked();
                if(checked)
                {
                     myEdit.putString("username",username.getText().toString());
                     myEdit.putString("password",password.getText().toString());
                     boolean commit=myEdit.commit();
                 }
                else
                {
                    myEdit.clear();
                }

            }
        });
    }
}