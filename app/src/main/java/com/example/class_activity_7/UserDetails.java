package com.example.class_activity_7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class UserDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        TextView username=findViewById(R.id.username);
        TextView loc=findViewById(R.id.loc);

        SharedPreferences prefs=getSharedPreferences("MySharedPref",MODE_PRIVATE);
    username.setText("Name: "+prefs.getString("username",""));

loc.setText("Current Location: \n Latitude: "+prefs.getString("latitude","")+" \nLongitude: "+prefs.getString("longitude","")+"\n");
    }
}